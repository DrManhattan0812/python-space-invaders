import pygame

class Player:
    xwingImg = "red_laser.png"
    xwingTxt = 0
    y = 550
    x = 550

    def __init__(self):
        self.xwingTxt = pygame.transform.scale(pygame.image.load("xwing.png"), (150,100))

    def setPlayerX(self, value):
        self.x = value

    def getPlayerX(self):
        return self.x

    def getPlayerY(self):
        return self.y

    def moveRight(self):
        self.x += 1

    def moveLeft(self):
        self.x -= 1