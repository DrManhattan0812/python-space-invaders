import pygame

class Missile:
    missiles = []
    missImg = "red_laser.png"
    missTxt = 0
    y = 0
    x = 0

# Faire de l'héritage ou polymorphisme car deux types de missiles.
    def __init__(self, playerX, playerY):
        self.missTxt = pygame.transform.scale(pygame.image.load(self.missImg), (5, 30))
        self.x = playerX
        self.y = playerY

    def missMove(self):
        self.y -= 3