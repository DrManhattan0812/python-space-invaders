import random
import pygame
import pygame.time
import datetime
from player import Player
from missile import Missile
from enemy import Enemy

class Game:

    def __init__(self):
        self.running = True
        self.screen = pygame.display.set_mode ((1200, 700))
        self.bgImg = pygame.transform.scale(pygame.image.load("bckImg.png"), (1200, 700))
        self.player = Player()
        self.moveR = False
        self.moveL = False
        self.shoot = False

    def update(self):

        # Déplacements du X-Wing
        if(self.moveR): 
            self.player.moveRight()
        if(self.moveL):
            self.player.moveLeft()

        # Déplacements des missiles
        if Missile.missiles:
            for missile in Missile.missiles:
                missile.missMove()
            
    def enemyShoots(self, actualTime, enemyShootingTime):

        if(actualTime > enemyShootingTime + datetime.timedelta(0,3)):
            
            Enemy.enemies[random.randint(0, len(Enemy.enemies) - 1)].shoot(Missile.missiles)
            enemyShootingTime = datetime.datetime.now()

        return enemyShootingTime
    def shootingEvent(self, actualTime, shootTime):
        
        # Gestion du tir et du délai de tir
        if(self.shoot) and (actualTime > shootTime + datetime.timedelta(0,3)):

            shootTime = datetime.datetime.now()
            Missile.missiles.append(Missile(self.player.getPlayerX(), self.player.getPlayerY()))
            Missile.missiles.append(Missile(self.player.getPlayerX() + 145, self.player.getPlayerY()))
            self.shoot = False
        
    def renderGame(self):

            self.screen.blit(self.bgImg, (0,0))
            self.screen.blit(self.player.xwingTxt, (self.player.getPlayerX(), self.player.getPlayerY()))

            for missile in Missile.missiles:
                self.screen.blit(missile.missTxt, (missile.x, missile.y))

            for enemy in Enemy.enemies:
                self.screen.blit(enemy.enemyTxt, (enemy.x, enemy.y))

    def killEvent(self):

        for enemy in Enemy.enemies:
            if enemy.isHurt(Missile.missiles):
                Enemy.enemies.remove(enemy)
                 
    def gameLoop(self):

        shootTime = datetime.datetime.now();
        enemyShootingTime = datetime.datetime.now();
        Enemy.generateEnemyLine()

        while self.running:

            # Gestion des événements (déplacer dans une classe à part) #

            for event in pygame.event.get():

                if event.type == pygame.QUIT:
                    self.running = False
                    Missile.missiles.clear()

                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_q:
                        self.moveL = True
                    if event.key == pygame.K_d:
                        self.moveR = True
                    if event.key == pygame.K_SPACE:
                        self.shoot = True

                if event.type == pygame.KEYUP:
                    if event.key == pygame.K_q:
                        self.moveL = False
                    if event.key == pygame.K_d:
                        self.moveR = False
                    if event.key == pygame.K_SPACE:
                        self.shoot = False

            self.killEvent()

            for enemy in Enemy.enemies:
                enemy.move()

            actualTime = datetime.datetime.now()
            enemyShootingTime = self.enemyShoots(actualTime, enemyShootingTime)
            if(self.shoot):
                self.shootingEvent(actualTime, shootTime)

            # Fin de la partie gestion des events

            self.update()
            self.renderGame()
            pygame.display.update()            
            pygame.time.delay(1)

        pygame.quit()
        exit()