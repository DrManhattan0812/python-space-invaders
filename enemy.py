import pygame
import random
import missile

class Enemy:
    enemies = []
    enemyImg = "TIE.png"
    enemyTxt = 0

    def __init__(self, x, y):
        self.enemyTxt = pygame.transform.scale(pygame.image.load(self.enemyImg), (90, 120))
        self.x = x
        self.y = y
        self.moveWay = False

    # Génère la ligne d'ennemis 
    # A modifier pour introduire de l'aléatoire
    def generateEnemyLine():

        nbEnemies = random.randrange(6, 12)
        
        for i in range (0, nbEnemies):
            x = random.randrange(100, 1000)
            y= random.randrange(10, 50)
            Enemy.enemies.append(Enemy(x, y))
    
    # Détection collision missile/ennemie
    def isHurt(self,missiles):
        for missile in missiles:
            if self.x < missile.x < self.x+100:
                if self.y < missile.y < self.y+150:
                    return True;

    def shoot(self, missiles):
        print("boom")

    def move(self):

        if self.moveWay == True:
            self.x += 2
        else:
            self.x -= 2

        if self.x >= 1200:
            self.moveWay = False
            self.y += 40
        elif self.x <= 0:
            self.moveWay = True
            self.y += 40